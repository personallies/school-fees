
package gh.com.dialect.transflow.webclient.integration.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import gh.com.dialect.transflow.webclient.dao.xsd.LogDAOSpringImpl;
import gh.com.dialect.transflow.webclient.integration.model.xsd.AppConstants;


/**
 * <p>Java class for BusinessLogic complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BusinessLogic">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appConstants" type="{http://model.integration.webclient.transflow.dialect.com.gh/xsd}AppConstants" minOccurs="0"/>
 *         &lt;element name="logDAOSpringImpl" type="{http://dao.webclient.transflow.dialect.com.gh/xsd}LogDAOSpringImpl" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusinessLogic", propOrder = {
    "appConstants",
    "logDAOSpringImpl"
})
public class BusinessLogic {

    @XmlElementRef(name = "appConstants", namespace = "http://integration.webclient.transflow.dialect.com.gh/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<AppConstants> appConstants;
    @XmlElementRef(name = "logDAOSpringImpl", namespace = "http://integration.webclient.transflow.dialect.com.gh/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<LogDAOSpringImpl> logDAOSpringImpl;

    /**
     * Gets the value of the appConstants property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AppConstants }{@code >}
     *     
     */
    public JAXBElement<AppConstants> getAppConstants() {
        return appConstants;
    }

    /**
     * Sets the value of the appConstants property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AppConstants }{@code >}
     *     
     */
    public void setAppConstants(JAXBElement<AppConstants> value) {
        this.appConstants = value;
    }

    /**
     * Gets the value of the logDAOSpringImpl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LogDAOSpringImpl }{@code >}
     *     
     */
    public JAXBElement<LogDAOSpringImpl> getLogDAOSpringImpl() {
        return logDAOSpringImpl;
    }

    /**
     * Sets the value of the logDAOSpringImpl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LogDAOSpringImpl }{@code >}
     *     
     */
    public void setLogDAOSpringImpl(JAXBElement<LogDAOSpringImpl> value) {
        this.logDAOSpringImpl = value;
    }

}
