
package gh.com.dialect.transflow.webclient.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gh.com.dialect.transflow.webclient.integration.model.xsd.GetTransResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://model.integration.webclient.transflow.dialect.com.gh/xsd}GetTransResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "getTransResponse")
public class GetTransResponse {

    @XmlElementRef(name = "return", namespace = "http://services.webclient.transflow.dialect.com.gh", type = JAXBElement.class, required = false)
    protected JAXBElement<GetTransResp> _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetTransResp }{@code >}
     *     
     */
    public JAXBElement<GetTransResp> getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetTransResp }{@code >}
     *     
     */
    public void setReturn(JAXBElement<GetTransResp> value) {
        this._return = value;
    }

}
