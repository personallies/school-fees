
package gh.com.dialect.transflow.webclient.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gh.com.dialect.transflow.webclient.integration.model.xsd.AppConstants;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="appConstants" type="{http://model.integration.webclient.transflow.dialect.com.gh/xsd}AppConstants" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "appConstants"
})
@XmlRootElement(name = "setAppConstants")
public class SetAppConstants {

    @XmlElementRef(name = "appConstants", namespace = "http://services.webclient.transflow.dialect.com.gh", type = JAXBElement.class, required = false)
    protected JAXBElement<AppConstants> appConstants;

    /**
     * Gets the value of the appConstants property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AppConstants }{@code >}
     *     
     */
    public JAXBElement<AppConstants> getAppConstants() {
        return appConstants;
    }

    /**
     * Sets the value of the appConstants property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AppConstants }{@code >}
     *     
     */
    public void setAppConstants(JAXBElement<AppConstants> value) {
        this.appConstants = value;
    }

}
