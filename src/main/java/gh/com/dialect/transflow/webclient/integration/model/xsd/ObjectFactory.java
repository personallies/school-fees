
package gh.com.dialect.transflow.webclient.integration.model.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gh.com.dialect.transflow.webclient.integration.model.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GeneralResponseResponseMessage_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "responseMessage");
    private final static QName _GeneralResponseResponseCode_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "responseCode");
    private final static QName _GetTransRespTransBatch_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "transBatch");
    private final static QName _AppConstantsWsdlLocation_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "wsdlLocation");
    private final static QName _PostInvRespInvoiceNo_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "invoiceNo");
    private final static QName _GetTransDataReceiptNo_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "receiptNo");
    private final static QName _GetTransDataTransDate_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "transDate");
    private final static QName _GetTransDataPayerName_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "payerName");
    private final static QName _GetTransDataCurrency_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "currency");
    private final static QName _GetTransDataSource_QNAME = new QName("http://model.integration.webclient.transflow.dialect.com.gh/xsd", "source");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gh.com.dialect.transflow.webclient.integration.model.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GeneralResponse }
     * 
     */
    public GeneralResponse createGeneralResponse() {
        return new GeneralResponse();
    }

    /**
     * Create an instance of {@link PostInvResp }
     * 
     */
    public PostInvResp createPostInvResp() {
        return new PostInvResp();
    }

    /**
     * Create an instance of {@link GetTransData }
     * 
     */
    public GetTransData createGetTransData() {
        return new GetTransData();
    }

    /**
     * Create an instance of {@link GetTransResp }
     * 
     */
    public GetTransResp createGetTransResp() {
        return new GetTransResp();
    }

    /**
     * Create an instance of {@link AppConstants }
     * 
     */
    public AppConstants createAppConstants() {
        return new AppConstants();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "responseMessage", scope = GeneralResponse.class)
    public JAXBElement<String> createGeneralResponseResponseMessage(String value) {
        return new JAXBElement<String>(_GeneralResponseResponseMessage_QNAME, String.class, GeneralResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "responseCode", scope = GeneralResponse.class)
    public JAXBElement<String> createGeneralResponseResponseCode(String value) {
        return new JAXBElement<String>(_GeneralResponseResponseCode_QNAME, String.class, GeneralResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "transBatch", scope = GetTransResp.class)
    public JAXBElement<String> createGetTransRespTransBatch(String value) {
        return new JAXBElement<String>(_GetTransRespTransBatch_QNAME, String.class, GetTransResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "wsdlLocation", scope = AppConstants.class)
    public JAXBElement<String> createAppConstantsWsdlLocation(String value) {
        return new JAXBElement<String>(_AppConstantsWsdlLocation_QNAME, String.class, AppConstants.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "invoiceNo", scope = PostInvResp.class)
    public JAXBElement<String> createPostInvRespInvoiceNo(String value) {
        return new JAXBElement<String>(_PostInvRespInvoiceNo_QNAME, String.class, PostInvResp.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "receiptNo", scope = GetTransData.class)
    public JAXBElement<String> createGetTransDataReceiptNo(String value) {
        return new JAXBElement<String>(_GetTransDataReceiptNo_QNAME, String.class, GetTransData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "transDate", scope = GetTransData.class)
    public JAXBElement<String> createGetTransDataTransDate(String value) {
        return new JAXBElement<String>(_GetTransDataTransDate_QNAME, String.class, GetTransData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "payerName", scope = GetTransData.class)
    public JAXBElement<String> createGetTransDataPayerName(String value) {
        return new JAXBElement<String>(_GetTransDataPayerName_QNAME, String.class, GetTransData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "currency", scope = GetTransData.class)
    public JAXBElement<String> createGetTransDataCurrency(String value) {
        return new JAXBElement<String>(_GetTransDataCurrency_QNAME, String.class, GetTransData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "source", scope = GetTransData.class)
    public JAXBElement<String> createGetTransDataSource(String value) {
        return new JAXBElement<String>(_GetTransDataSource_QNAME, String.class, GetTransData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", name = "invoiceNo", scope = GetTransData.class)
    public JAXBElement<String> createGetTransDataInvoiceNo(String value) {
        return new JAXBElement<String>(_PostInvRespInvoiceNo_QNAME, String.class, GetTransData.class, value);
    }

}
