
package gh.com.dialect.transflow.webclient.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gh.com.dialect.transflow.webclient.integration.xsd.BusinessLogic;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="businessLogic" type="{http://integration.webclient.transflow.dialect.com.gh/xsd}BusinessLogic" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "businessLogic"
})
@XmlRootElement(name = "setBusinessLogic")
public class SetBusinessLogic {

    @XmlElementRef(name = "businessLogic", namespace = "http://services.webclient.transflow.dialect.com.gh", type = JAXBElement.class, required = false)
    protected JAXBElement<BusinessLogic> businessLogic;

    /**
     * Gets the value of the businessLogic property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BusinessLogic }{@code >}
     *     
     */
    public JAXBElement<BusinessLogic> getBusinessLogic() {
        return businessLogic;
    }

    /**
     * Sets the value of the businessLogic property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BusinessLogic }{@code >}
     *     
     */
    public void setBusinessLogic(JAXBElement<BusinessLogic> value) {
        this.businessLogic = value;
    }

}
