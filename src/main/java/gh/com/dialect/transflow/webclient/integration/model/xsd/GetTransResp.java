
package gh.com.dialect.transflow.webclient.integration.model.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetTransResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetTransResp">
 *   &lt;complexContent>
 *     &lt;extension base="{http://model.integration.webclient.transflow.dialect.com.gh/xsd}GeneralResponse">
 *       &lt;sequence>
 *         &lt;element name="getTransDataArray" type="{http://model.integration.webclient.transflow.dialect.com.gh/xsd}GetTransData" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="transBatch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetTransResp", propOrder = {
    "getTransDataArray",
    "transBatch"
})
public class GetTransResp
    extends GeneralResponse
{

    @XmlElement(nillable = true)
    protected List<GetTransData> getTransDataArray;
    @XmlElementRef(name = "transBatch", namespace = "http://model.integration.webclient.transflow.dialect.com.gh/xsd", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transBatch;

    /**
     * Gets the value of the getTransDataArray property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getTransDataArray property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetTransDataArray().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetTransData }
     * 
     * 
     */
    public List<GetTransData> getGetTransDataArray() {
        if (getTransDataArray == null) {
            getTransDataArray = new ArrayList<GetTransData>();
        }
        return this.getTransDataArray;
    }

    /**
     * Gets the value of the transBatch property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransBatch() {
        return transBatch;
    }

    /**
     * Sets the value of the transBatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransBatch(JAXBElement<String> value) {
        this.transBatch = value;
    }

}
