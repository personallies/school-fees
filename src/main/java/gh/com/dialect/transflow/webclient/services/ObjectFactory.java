
package gh.com.dialect.transflow.webclient.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import gh.com.dialect.transflow.webclient.integration.model.xsd.AppConstants;
import gh.com.dialect.transflow.webclient.integration.model.xsd.GeneralResponse;
import gh.com.dialect.transflow.webclient.integration.model.xsd.GetTransResp;
import gh.com.dialect.transflow.webclient.integration.model.xsd.PostInvResp;
import gh.com.dialect.transflow.webclient.integration.xsd.BusinessLogic;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gh.com.dialect.transflow.webclient.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PostInvoiceResponseReturn_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "return");
    private final static QName _ResetTransPassword_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "password");
    private final static QName _ResetTransResetDate_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "resetDate");
    private final static QName _ResetTransUsername_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "username");
    private final static QName _SendSMSSrc_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "src");
    private final static QName _SendSMSMobile_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "mobile");
    private final static QName _SendSMSMesg_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "mesg");
    private final static QName _CheckInvStatusInvoiceNo_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "invoiceNo");
    private final static QName _SetAppConstantsAppConstants_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "appConstants");
    private final static QName _SetBusinessLogicBusinessLogic_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "businessLogic");
    private final static QName _ConfirmTransTransBatch_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "transBatch");
    private final static QName _CheckBillStatusTransDate_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "transDate");
    private final static QName _CheckBillStatusThirdpartyID_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "thirdpartyID");
    private final static QName _PostInvoiceBillprompt_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "billprompt");
    private final static QName _PostInvoiceExpiry_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "expiry");
    private final static QName _PostInvoiceName_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "name");
    private final static QName _PostInvoiceInfo_QNAME = new QName("http://services.webclient.transflow.dialect.com.gh", "info");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gh.com.dialect.transflow.webclient.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CheckBillStatusResponse }
     * 
     */
    public CheckBillStatusResponse createCheckBillStatusResponse() {
        return new CheckBillStatusResponse();
    }

    /**
     * Create an instance of {@link CancelInvoiceResponse }
     * 
     */
    public CancelInvoiceResponse createCancelInvoiceResponse() {
        return new CancelInvoiceResponse();
    }

    /**
     * Create an instance of {@link ConfirmTransResponse }
     * 
     */
    public ConfirmTransResponse createConfirmTransResponse() {
        return new ConfirmTransResponse();
    }

    /**
     * Create an instance of {@link CheckBillStatus }
     * 
     */
    public CheckBillStatus createCheckBillStatus() {
        return new CheckBillStatus();
    }

    /**
     * Create an instance of {@link CancelInvoice }
     * 
     */
    public CancelInvoice createCancelInvoice() {
        return new CancelInvoice();
    }

    /**
     * Create an instance of {@link GetTrans }
     * 
     */
    public GetTrans createGetTrans() {
        return new GetTrans();
    }

    /**
     * Create an instance of {@link SetBusinessLogic }
     * 
     */
    public SetBusinessLogic createSetBusinessLogic() {
        return new SetBusinessLogic();
    }

    /**
     * Create an instance of {@link CheckInvStatus }
     * 
     */
    public CheckInvStatus createCheckInvStatus() {
        return new CheckInvStatus();
    }

    /**
     * Create an instance of {@link ResetTransResponse }
     * 
     */
    public ResetTransResponse createResetTransResponse() {
        return new ResetTransResponse();
    }

    /**
     * Create an instance of {@link GetTransResponse }
     * 
     */
    public GetTransResponse createGetTransResponse() {
        return new GetTransResponse();
    }

    /**
     * Create an instance of {@link PostInvoiceResponse }
     * 
     */
    public PostInvoiceResponse createPostInvoiceResponse() {
        return new PostInvoiceResponse();
    }

    /**
     * Create an instance of {@link ConfirmTrans }
     * 
     */
    public ConfirmTrans createConfirmTrans() {
        return new ConfirmTrans();
    }

    /**
     * Create an instance of {@link ResetTrans }
     * 
     */
    public ResetTrans createResetTrans() {
        return new ResetTrans();
    }

    /**
     * Create an instance of {@link SetAppConstants }
     * 
     */
    public SetAppConstants createSetAppConstants() {
        return new SetAppConstants();
    }

    /**
     * Create an instance of {@link SendSMS }
     * 
     */
    public SendSMS createSendSMS() {
        return new SendSMS();
    }

    /**
     * Create an instance of {@link SendSMSResponse }
     * 
     */
    public SendSMSResponse createSendSMSResponse() {
        return new SendSMSResponse();
    }

    /**
     * Create an instance of {@link CheckInvStatusResponse }
     * 
     */
    public CheckInvStatusResponse createCheckInvStatusResponse() {
        return new CheckInvStatusResponse();
    }

    /**
     * Create an instance of {@link PostInvoice }
     * 
     */
    public PostInvoice createPostInvoice() {
        return new PostInvoice();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostInvResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = PostInvoiceResponse.class)
    public JAXBElement<PostInvResp> createPostInvoiceResponseReturn(PostInvResp value) {
        return new JAXBElement<PostInvResp>(_PostInvoiceResponseReturn_QNAME, PostInvResp.class, PostInvoiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = ResetTrans.class)
    public JAXBElement<String> createResetTransPassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, ResetTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "resetDate", scope = ResetTrans.class)
    public JAXBElement<String> createResetTransResetDate(String value) {
        return new JAXBElement<String>(_ResetTransResetDate_QNAME, String.class, ResetTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = ResetTrans.class)
    public JAXBElement<String> createResetTransUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, ResetTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = CheckInvStatusResponse.class)
    public JAXBElement<GeneralResponse> createCheckInvStatusResponseReturn(GeneralResponse value) {
        return new JAXBElement<GeneralResponse>(_PostInvoiceResponseReturn_QNAME, GeneralResponse.class, CheckInvStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = GetTrans.class)
    public JAXBElement<String> createGetTransPassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, GetTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = GetTrans.class)
    public JAXBElement<String> createGetTransUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, GetTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = CheckBillStatusResponse.class)
    public JAXBElement<GeneralResponse> createCheckBillStatusResponseReturn(GeneralResponse value) {
        return new JAXBElement<GeneralResponse>(_PostInvoiceResponseReturn_QNAME, GeneralResponse.class, CheckBillStatusResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransResp }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = GetTransResponse.class)
    public JAXBElement<GetTransResp> createGetTransResponseReturn(GetTransResp value) {
        return new JAXBElement<GetTransResp>(_PostInvoiceResponseReturn_QNAME, GetTransResp.class, GetTransResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = SendSMS.class)
    public JAXBElement<String> createSendSMSPassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, SendSMS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "src", scope = SendSMS.class)
    public JAXBElement<String> createSendSMSSrc(String value) {
        return new JAXBElement<String>(_SendSMSSrc_QNAME, String.class, SendSMS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "mobile", scope = SendSMS.class)
    public JAXBElement<String> createSendSMSMobile(String value) {
        return new JAXBElement<String>(_SendSMSMobile_QNAME, String.class, SendSMS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "mesg", scope = SendSMS.class)
    public JAXBElement<String> createSendSMSMesg(String value) {
        return new JAXBElement<String>(_SendSMSMesg_QNAME, String.class, SendSMS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = SendSMS.class)
    public JAXBElement<String> createSendSMSUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, SendSMS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = CheckInvStatus.class)
    public JAXBElement<String> createCheckInvStatusPassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, CheckInvStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = CheckInvStatus.class)
    public JAXBElement<String> createCheckInvStatusUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, CheckInvStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "invoiceNo", scope = CheckInvStatus.class)
    public JAXBElement<String> createCheckInvStatusInvoiceNo(String value) {
        return new JAXBElement<String>(_CheckInvStatusInvoiceNo_QNAME, String.class, CheckInvStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AppConstants }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "appConstants", scope = SetAppConstants.class)
    public JAXBElement<AppConstants> createSetAppConstantsAppConstants(AppConstants value) {
        return new JAXBElement<AppConstants>(_SetAppConstantsAppConstants_QNAME, AppConstants.class, SetAppConstants.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = ConfirmTransResponse.class)
    public JAXBElement<GeneralResponse> createConfirmTransResponseReturn(GeneralResponse value) {
        return new JAXBElement<GeneralResponse>(_PostInvoiceResponseReturn_QNAME, GeneralResponse.class, ConfirmTransResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = ResetTransResponse.class)
    public JAXBElement<GeneralResponse> createResetTransResponseReturn(GeneralResponse value) {
        return new JAXBElement<GeneralResponse>(_PostInvoiceResponseReturn_QNAME, GeneralResponse.class, ResetTransResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = SendSMSResponse.class)
    public JAXBElement<GeneralResponse> createSendSMSResponseReturn(GeneralResponse value) {
        return new JAXBElement<GeneralResponse>(_PostInvoiceResponseReturn_QNAME, GeneralResponse.class, SendSMSResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BusinessLogic }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "businessLogic", scope = SetBusinessLogic.class)
    public JAXBElement<BusinessLogic> createSetBusinessLogicBusinessLogic(BusinessLogic value) {
        return new JAXBElement<BusinessLogic>(_SetBusinessLogicBusinessLogic_QNAME, BusinessLogic.class, SetBusinessLogic.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = ConfirmTrans.class)
    public JAXBElement<String> createConfirmTransPassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, ConfirmTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "transBatch", scope = ConfirmTrans.class)
    public JAXBElement<String> createConfirmTransTransBatch(String value) {
        return new JAXBElement<String>(_ConfirmTransTransBatch_QNAME, String.class, ConfirmTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = ConfirmTrans.class)
    public JAXBElement<String> createConfirmTransUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, ConfirmTrans.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = CancelInvoice.class)
    public JAXBElement<String> createCancelInvoicePassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, CancelInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = CancelInvoice.class)
    public JAXBElement<String> createCancelInvoiceUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, CancelInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "invoiceNo", scope = CancelInvoice.class)
    public JAXBElement<String> createCancelInvoiceInvoiceNo(String value) {
        return new JAXBElement<String>(_CheckInvStatusInvoiceNo_QNAME, String.class, CancelInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = CheckBillStatus.class)
    public JAXBElement<String> createCheckBillStatusPassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, CheckBillStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "mobile", scope = CheckBillStatus.class)
    public JAXBElement<String> createCheckBillStatusMobile(String value) {
        return new JAXBElement<String>(_SendSMSMobile_QNAME, String.class, CheckBillStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "transDate", scope = CheckBillStatus.class)
    public JAXBElement<String> createCheckBillStatusTransDate(String value) {
        return new JAXBElement<String>(_CheckBillStatusTransDate_QNAME, String.class, CheckBillStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "thirdpartyID", scope = CheckBillStatus.class)
    public JAXBElement<String> createCheckBillStatusThirdpartyID(String value) {
        return new JAXBElement<String>(_CheckBillStatusThirdpartyID_QNAME, String.class, CheckBillStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = CheckBillStatus.class)
    public JAXBElement<String> createCheckBillStatusUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, CheckBillStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneralResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "return", scope = CancelInvoiceResponse.class)
    public JAXBElement<GeneralResponse> createCancelInvoiceResponseReturn(GeneralResponse value) {
        return new JAXBElement<GeneralResponse>(_PostInvoiceResponseReturn_QNAME, GeneralResponse.class, CancelInvoiceResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "password", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoicePassword(String value) {
        return new JAXBElement<String>(_ResetTransPassword_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "billprompt", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceBillprompt(String value) {
        return new JAXBElement<String>(_PostInvoiceBillprompt_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "expiry", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceExpiry(String value) {
        return new JAXBElement<String>(_PostInvoiceExpiry_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "name", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceName(String value) {
        return new JAXBElement<String>(_PostInvoiceName_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "mobile", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceMobile(String value) {
        return new JAXBElement<String>(_SendSMSMobile_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "thirdpartyID", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceThirdpartyID(String value) {
        return new JAXBElement<String>(_CheckBillStatusThirdpartyID_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "info", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceInfo(String value) {
        return new JAXBElement<String>(_PostInvoiceInfo_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "mesg", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceMesg(String value) {
        return new JAXBElement<String>(_SendSMSMesg_QNAME, String.class, PostInvoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.webclient.transflow.dialect.com.gh", name = "username", scope = PostInvoice.class)
    public JAXBElement<String> createPostInvoiceUsername(String value) {
        return new JAXBElement<String>(_ResetTransUsername_QNAME, String.class, PostInvoice.class, value);
    }

}
