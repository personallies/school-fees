
package gh.com.dialect.transflow.webclient.integration.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import gh.com.dialect.transflow.webclient.dao.xsd.LogDAOSpringImpl;
import gh.com.dialect.transflow.webclient.integration.model.xsd.AppConstants;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gh.com.dialect.transflow.webclient.integration.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BusinessLogicLogDAOSpringImpl_QNAME = new QName("http://integration.webclient.transflow.dialect.com.gh/xsd", "logDAOSpringImpl");
    private final static QName _BusinessLogicAppConstants_QNAME = new QName("http://integration.webclient.transflow.dialect.com.gh/xsd", "appConstants");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gh.com.dialect.transflow.webclient.integration.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BusinessLogic }
     * 
     */
    public BusinessLogic createBusinessLogic() {
        return new BusinessLogic();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogDAOSpringImpl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.webclient.transflow.dialect.com.gh/xsd", name = "logDAOSpringImpl", scope = BusinessLogic.class)
    public JAXBElement<LogDAOSpringImpl> createBusinessLogicLogDAOSpringImpl(LogDAOSpringImpl value) {
        return new JAXBElement<LogDAOSpringImpl>(_BusinessLogicLogDAOSpringImpl_QNAME, LogDAOSpringImpl.class, BusinessLogic.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AppConstants }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://integration.webclient.transflow.dialect.com.gh/xsd", name = "appConstants", scope = BusinessLogic.class)
    public JAXBElement<AppConstants> createBusinessLogicAppConstants(AppConstants value) {
        return new JAXBElement<AppConstants>(_BusinessLogicAppConstants_QNAME, AppConstants.class, BusinessLogic.class, value);
    }

}
