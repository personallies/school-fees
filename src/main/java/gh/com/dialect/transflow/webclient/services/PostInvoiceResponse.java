
package gh.com.dialect.transflow.webclient.services;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import gh.com.dialect.transflow.webclient.integration.model.xsd.PostInvResp;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://model.integration.webclient.transflow.dialect.com.gh/xsd}PostInvResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "postInvoiceResponse")
public class PostInvoiceResponse {

    @XmlElementRef(name = "return", namespace = "http://services.webclient.transflow.dialect.com.gh", type = JAXBElement.class, required = false)
    protected JAXBElement<PostInvResp> _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PostInvResp }{@code >}
     *     
     */
    public JAXBElement<PostInvResp> getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PostInvResp }{@code >}
     *     
     */
    public void setReturn(JAXBElement<PostInvResp> value) {
        this._return = value;
    }

}
