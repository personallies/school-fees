package com.itconsortiumgh.school.payments.model;

import java.util.List;

import lombok.Data;

@Data
public class SchAliasBody extends GeneralResponse{
	private List<SchAliasData> data;
}
