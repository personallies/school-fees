package com.itconsortiumgh.school.payments.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;


@Data
@Table
@Entity
public class UssdSession implements DomainObject{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	//USSD Data
	@Column(length=12)
	private String msisdn;
	@Column(length=1)
	private String newRequest;
	@Column(length=2)
	private String mode;
	@Column
	private String sessionId;
	@Column(length=20)
	private String subscriberInput;
	@Column(length=20)
	private String clientState;
	@Column(length=20)
	private BigDecimal amount;
	@Column(length=20)
	private String paymentType;
	@Column(length=20)
	private String searchCriteria;
	@Column
	private String schoolName;
	@Column(length=20)
	private String schoolAlias;
	@Column(length=20)
	private String studentId;
	@Column 
	private String studentName;
	@Column(length=20)
	private String merchantId;
	@Column(length=20)
	private String productId;
	@Column(length=20)
	private String serviceCode;
	@Column
	private String productName;
	@Column
	private String apiIndex;
	@Transient
	private String header;
	@Transient
	private Map<Integer, String> productsMap;
	@Transient
	private Map<Integer, String> schoolsMap;
	
	private static final long serialVersionUID = 1L;
	public static final String OBJECT_KEY = "ScP_";
	
	public UssdSession() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UssdSession(String sessionId){
		super();
		this.sessionId = OBJECT_KEY+sessionId; 
	}
	
	@Override
	public String getKey() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		if(!sessionId.startsWith("ScP_")){
			return sb.append(sessionId).toString();
		}else{
			return sessionId;
		}
	}
	
	public void setKey(String sessionId){
		if(sessionId.startsWith("ScP_")){
			this.sessionId = sessionId;
		}else{
			this.sessionId = OBJECT_KEY+sessionId;
		}
	}

	@Override
	public String getObjectKey() {
		// TODO Auto-generated method stub
		return OBJECT_KEY;
	}

}
