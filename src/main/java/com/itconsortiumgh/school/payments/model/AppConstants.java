package com.itconsortiumgh.school.payments.model;

public class AppConstants {
	public static final String FREELOW_STATE_CONTINUE = "FC";
	public static final String FREELOW_STATE_END = "FB";
	public static final String NEW_REQUEST = "1";
	public static final String CONTINUING_REQUEST = "0";
	public static final String FAILURE		= "failure";
	public static final String SUCCESS		= "success";
	public static final String PAY_FEES = "payFees";
	public static final String SCHEDULE_FEES = "scheduleFees";
	public static final String SEARCH_BY_ALIAS = "searchByAlias";
	public static final String SEARCH_BY_NAME = "searchByName";
	public static final String TRANSFLOW_SUCCESS = "0000";
}
