package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class StdIdData {
	private String name;
	private String accountRef;
	private String programme;
}
