package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class SchAliasResp {
	private String header;
	private SchAliasBody body;
}
