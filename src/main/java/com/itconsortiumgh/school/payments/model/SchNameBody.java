package com.itconsortiumgh.school.payments.model;

import java.util.List;

import lombok.Data;

@Data
public class SchNameBody extends GeneralResponse{
	private List<SchNameData> data = null;
}
