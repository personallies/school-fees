package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class StdIdResp {
	private String header;
	private StdIdBody body;
}
