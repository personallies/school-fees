package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class SchNameResp {
	private String header;
	private SchNameBody body;

}
