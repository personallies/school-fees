package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class InvoiceResponse {
	private String invoiceNo;
	private String responseCode;
	private String responseMessage;
}
