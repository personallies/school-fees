package com.itconsortiumgh.school.payments.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class GeneralResponse {
	public static final String CODE_01_OK = "01";
	public static final String CODE_06_INACTIVE_SCHOOL= "06";
	public static final String CODE_09_INVALID_SCHOOL= "09";
	private String responseMessage;
	private String responseCode;
	

}
