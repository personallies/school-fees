package com.itconsortiumgh.school.payments.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@XmlRootElement(name = "response")
@Component
public class UssdResponse {
	private String msisdn;
	private String applicationResponse;
	private Freeflow freeflow;
	
	@XmlElement
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	
	@XmlElement
	public void setApplicationResponse(String applicationResponse) {
		this.applicationResponse = applicationResponse;
	}
	
	@XmlElement
	public void setFreeflow(Freeflow freeflow) {
		this.freeflow = freeflow;
	}
}
