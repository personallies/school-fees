package com.itconsortiumgh.school.payments.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@XmlRootElement(name = "request")
public class UssdRequest {
	private String msisdn;
	private String newRequest;
	private Freeflow freeflow;
	private String sessionId;
	private String subscriberInput;
	private String type;

	@XmlElement
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@XmlElement
	public void setNewRequest(String newRequest) {
		this.newRequest = newRequest;
	}

	@XmlElement
	public void setFreeflow(Freeflow freeflow) {
		this.freeflow = freeflow;
	}

	@XmlElement
	public void setType(String type) {
		this.type = type;
	}
	
	
}
