package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class SchAliasData {
	private String merchantName;
	private String merchantId;
	private String productId;
	private String apiIndex;
	private String serviceCode;
	private String productName;
}
