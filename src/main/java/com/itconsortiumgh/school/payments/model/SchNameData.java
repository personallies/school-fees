package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class SchNameData {
	private String merchantName;
	private String merchantId;
}
