package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class StdIdReq {
	private String productId;
	private String merchantId;
	private String apiIndex;
	private String source;
	private String serviceCode;
	private String accountRef;
}
