package com.itconsortiumgh.school.payments.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement(name = "freeflow")
public class Freeflow {
	private String mode;
	private String freeflowState;

	public Freeflow(String freeflowState) {
		super();
		this.freeflowState = freeflowState;
	}
	
	@XmlElement
	public void setMode(String mode) {
		this.mode = mode;
	}

	@XmlElement
	public void setFreeflowState(String freeflowState) {
		this.freeflowState = freeflowState;
	}
	
	public static Freeflow createTerminalFreeflow(){
		return new Freeflow("FD");
	}
	
	public static Freeflow createContinuousFreeflow(){
		return new Freeflow("FC");
	}

	public Freeflow() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
