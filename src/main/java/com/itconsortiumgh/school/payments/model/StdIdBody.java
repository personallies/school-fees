package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class StdIdBody extends GeneralResponse{
	private StdIdData data;
}
