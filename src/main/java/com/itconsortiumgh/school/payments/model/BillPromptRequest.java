package com.itconsortiumgh.school.payments.model;

import lombok.Data;

@Data
public class BillPromptRequest {
	private String name;
	private String info;
	private String amt;
	private String mobile;
	private String mesg;
	private String expiry;
	private String billprompt;
	private String thirdpartyID;
	private String username;
	private String password;
	
}
