package com.itconsortiumgh.school.payments.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Table
@Data
@Entity
@Slf4j
public class TransactionLogs {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	// START TRANSACTIONAL DATA
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;
	@Column(length = 50)
	private String merchantId;
	@Column
	private String schoolName;
	@Column(length = 50)
	private String productId;
	@Column(length = 50)
	private String productName;
	@Column(length = 50)
	private String msisdn;
	@Column(precision = 12, scale = 2)
	private BigDecimal amount;
	@Column(length = 50)
	private String transactionId;
	@Column(length = 50)
	private String thirdPartyTransactionId;
	@Column(length = 50)
	private String invoiceNo;
	@Column(length = 50)
	private String responseCode;
	@Column(length = 50)
	private String responseMessage;
	@Transient
	private UssdSession ussdSession;

	public TransactionLogs() {

	}

	public TransactionLogs(UssdSession ussdSession) {
		this.setMerchantId(ussdSession.getMerchantId());
		this.setSchoolName(ussdSession.getSchoolName());
		this.setProductId(ussdSession.getProductId());
		this.setProductName(ussdSession.getProductName());
		this.setMsisdn(ussdSession.getMsisdn());
		this.setAmount(ussdSession.getAmount());
		this.setTransactionId(ussdSession.getSessionId());
		this.setCreated(new Date());
		this.setUssdSession(ussdSession);
	}	
}
