package com.itconsortiumgh.school.payments.model;

//Client
public class ClientState {
	public static final String ROOT = "root";
	public static final String DISPLAY_FIRST_MENU = "displayFirstMenu";
	public static final String ENTER_SCHOOL_ALIAS = "enterSchoolAlias";
	public static final String ENTER_SCHOOL_NAME = "enterSchoolName";
	public static final String CHOOSE_SERVICE = "chooseService";
	public static final String ENTER_STUDENT_ID = "enterStudentId";
	public static final String ENTER_AMOUNT = "enterAmount";
	public static final String CONFIRM_PAYMENT = "confirmPayment";
	public static final String SCHEDULE_FEES = "scheduleFees";
	public static final String CHOOSE_RECENT = "chooseRecent";
	public static final String CHOOSE_SCHOOL = "chooseSchool";
	public static final String DISPLAY_SCHOOL_INVALID = "schoolInvalid";
	public static final String DISPLAY_SCHOOL_INACTIVE = "schoolInactive";
	public static final String DISPLAY_STUDENT_ID_INVALID = "studentIdInvalid";
	public static final String DISPLAY_STUDENT_INACTIVE = "studentInactive";
	public static final String APPROVE_BILLPROMPT = "approveBillPrompt";
	public static final String DISPLAY_CANCEL_MESSAGE = "cancelMessage";
	public static final String SCHOOL_ALIAS_ERROR = "schoolAliasError";
	public static final String SCHOOL_NAME_ERROR = "schoolNameError";
	public static final String STUDENT_ERROR = "studentError";
	public static final String AMOUNT_ERROR = "amountError";
	public static final String ALIAS_SERVICE_ERROR = "aliasServiceError";
	public static final String NAME_SERVICE_ERROR = "nameServiceError";
	public static final String MORE_SCHOOLS = "moreSchools";
	
}
