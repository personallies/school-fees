package com.itconsortiumgh.school.payments.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

@Data
@Configuration
@PropertySource("classpath:menu.properties")
@ConfigurationProperties
public class MenuProperties {
	private String displayFirstMenu;
	private String enterSchoolAlias;
	private String enterSchoolName;
	private String chooseService;
	private String enterStudenId;
	private String enterAmount;
	private String confirmPayment;
	private String scheduleFees;
	private String chooseRecent;
	private String chooseSchool;
	private String displaySchoolInvalid;
	private String displaySchoolInactive;
	private String displayStudentIdInvalid;
	private String displayStudentInactive;
	private String approveBillPrompt;
	private String displayCancelMessage;
	private String displayUnavailableSession;
	private String unknownClientState;
	private String displayEnterValidAmount;
	private String displayTryAgain;
	private String displayInvalidInput;
	private String displayThankYou;
	private String displayEnterSchoolAliasAgain;
	private String displayEnterSchoolNameAgain;
	private String displayEnterStudentIdAgain;
	private String displayEnterAmountAgain;
	private String displayCancel;
	private String displayMoreOption;
}
