package com.itconsortiumgh.school.payments.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties
public class ApplicationProperties {
	private String endPoint;
	private long ussdSessionTimeout;
	private String currencyCode;
	private String searchBySchNameUrl;
	private String searchBySchAliasUrl;
	private String searchByStdIdUrl;
	private String displaySchListUrl;
	private String searchForServByMerchId;
	private String network;
	
	private String transflowUrl;
	private String serviceName;
	private String narration;
	private String smsTemplate;
	private String billPrompt;
	private String thirdPartyID;
	private String username;
	private String password;
	private int expiryDays;
	
	private String xmlLocation;
	private String delayBeforePush;
	private String currentLocale;
	
}
