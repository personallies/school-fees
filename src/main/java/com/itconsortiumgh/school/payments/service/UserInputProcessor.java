package com.itconsortiumgh.school.payments.service;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.number.NumberFormatter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.school.payments.model.AppConstants;
import com.itconsortiumgh.school.payments.model.ClientState;
import com.itconsortiumgh.school.payments.model.Freeflow;
import com.itconsortiumgh.school.payments.model.GeneralResponse;
import com.itconsortiumgh.school.payments.model.SchAliasData;
import com.itconsortiumgh.school.payments.model.SchAliasResp;
import com.itconsortiumgh.school.payments.model.SchNameData;
import com.itconsortiumgh.school.payments.model.SchNameResp;
import com.itconsortiumgh.school.payments.model.StdIdReq;
import com.itconsortiumgh.school.payments.model.StdIdResp;
import com.itconsortiumgh.school.payments.model.UssdResponse;
import com.itconsortiumgh.school.payments.model.UssdSession;
import com.itconsortiumgh.school.payments.properties.ApplicationProperties;
import com.itconsortiumgh.school.payments.properties.MenuProperties;
import com.itconsortiumgh.school.payments.queue.RabbitMQPublisher;
import com.itconsortiumgh.school.payments.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.school.payments.utils.JsonUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserInputProcessor {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	MenuProperties menuProperties;
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	RedisUssdSessionRepository redisUssdSessionRepository;
	@Autowired
	RabbitMQPublisher rabbitMQPublisher;

	public UssdResponse askUserToChooseSearchCriteria(UssdSession ussdSession) {
		UssdResponse ussdResponse = new UssdResponse();
		ussdResponse.setApplicationResponse(menuProperties.getDisplayFirstMenu());
		ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
		ussdResponse.setMsisdn(ussdSession.getMsisdn());

		ussdSession.setClientState(ClientState.DISPLAY_FIRST_MENU);
		redisUssdSessionRepository.put(ussdSession);
		return ussdResponse;
	}

	public UssdResponse askUserToEnterSearchInput(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();
		ussdSession.setSubscriberInput(userInput);

		switch (userInput) {
		// Enter School ID
		case "1": {
			ussdResponse.setApplicationResponse(menuProperties.getEnterSchoolAlias());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setSearchCriteria(AppConstants.SEARCH_BY_ALIAS);
			ussdSession.setClientState(ClientState.ENTER_SCHOOL_ALIAS);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// Enter School Name
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getEnterSchoolName());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setSearchCriteria(AppConstants.SEARCH_BY_NAME);
			ussdSession.setClientState(ClientState.ENTER_SCHOOL_NAME);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// case "3"://Recent
			// ussdResponse.setApplicationResponse(menuProperties.getChooseRecent());
			// ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			// ussdResponse.setMsisdn(msisdn);
			//
			// ussdSession.setClientState(ClientState.CHOOSE_RECENT);
			// redisUssdSessionRepository.put(ussdSession);
			// break;

		default: {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput() + "\n"
					+ menuProperties.getDisplayFirstMenu() + menuProperties.getDisplayCancel());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_FIRST_MENU);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		}
		return ussdResponse;
	}

	public UssdResponse askUserToChooseServiceUsingSchAlias(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		Map<Integer, String> productsMap = new HashMap<>();

		ussdSession.setSubscriberInput(userInput);

		String header = "";
		String schoolName = "";
		String merchantId = "";
		String serviceCode = "";
		String productId = "";
		String productName = "";
		String responseMessage = "";
		String apiIndex = "";
		String msisdn = ussdSession.getMsisdn();
		String schAliasUrl = applicationProperties.getSearchBySchAliasUrl();
		String servUrl = applicationProperties.getSearchForServByMerchId();

		switch (userInput) {
		case "0": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		default: {
			// Hit API for merchantName, merchantId, productId, sericeCode
			SchAliasResp merchNameResp = restTemplate.getForObject(schAliasUrl + userInput, SchAliasResp.class);
			log.info("response from sadat {}", merchNameResp);
			header = merchNameResp.getHeader();
			responseMessage = merchNameResp.getBody().getResponseMessage();
			if (GeneralResponse.CODE_01_OK.equalsIgnoreCase(merchNameResp.getBody().getResponseCode())) {
				List<SchAliasData> schAliasDataList = merchNameResp.getBody().getData();

				for (int i = 0; i < schAliasDataList.size(); i++) {
					// Can't do an increment because only one value is expected
					// from the endpoint - so write or overwrite
					schoolName = schAliasDataList.get(i).getMerchantName();
					merchantId = schAliasDataList.get(i).getMerchantId();
					serviceCode = schAliasDataList.get(i).getServiceCode();
					productId = schAliasDataList.get(i).getProductId();
					productName = schAliasDataList.get(i).getProductName();
				}

				// Hit API with merchantId for productNames
				SchAliasResp servListResp = restTemplate.getForObject(servUrl + merchantId, SchAliasResp.class);
				log.info("response from sadat {}", servListResp);

				if (GeneralResponse.CODE_01_OK.equalsIgnoreCase(servListResp.getBody().getResponseCode())) {

					List<SchAliasData> servList = servListResp.getBody().getData();
					if (servList.size() != 1) {
						for (int j = 0; j < servList.size(); j++) {
							productId += servList.get(j).getProductId();
							productName += "\n" + (j + 1) + "." + servList.get(j).getProductName();

							// Put products into products map and save in
							// session
							productsMap.put((j + 1), servList.get(j).getProductName());
						}

						ussdResponse.setApplicationResponse(header + "\n" + schoolName + productName);
						ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
						ussdResponse.setMsisdn(msisdn);

						ussdSession.setHeader(header);
						ussdSession.setSchoolAlias(userInput);
						ussdSession.setSchoolName(schoolName);
						ussdSession.setApiIndex(apiIndex);
						ussdSession.setMerchantId(merchantId);
						ussdSession.setServiceCode(serviceCode);
						ussdSession.setProductsMap(productsMap);
						ussdSession.setClientState(ClientState.CHOOSE_SERVICE);
						redisUssdSessionRepository.put(ussdSession);
					} else if (servList.size() == 1) {
						ussdResponse.setApplicationResponse(
								MessageFormat.format(menuProperties.getEnterStudenId(), schoolName, productName));
						ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
						ussdResponse.setMsisdn(msisdn);

						ussdSession.setSchoolAlias(userInput);
						ussdSession.setSchoolName(schoolName);
						ussdSession.setApiIndex(apiIndex);
						ussdSession.setMerchantId(merchantId);
						ussdSession.setServiceCode(serviceCode);
						ussdSession.setProductId(productId);
						ussdSession.setProductName(productName);
						ussdSession.setClientState(ClientState.ENTER_STUDENT_ID);
						redisUssdSessionRepository.put(ussdSession);
					}
				}
			} else {
				ussdResponse.setApplicationResponse(menuProperties.getDisplayEnterSchoolAliasAgain());
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.ENTER_SCHOOL_ALIAS);
				redisUssdSessionRepository.put(ussdSession);
			}
			break;
		}
		}

		return ussdResponse;
	}

	public UssdResponse askUserToChooseSchool(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		Map<Integer, String> schoolsMap = new HashMap<>();
		int schoolSize;
		ussdSession.setSubscriberInput(userInput);

		String tmpSch [];
		String schoolName = "";
		String tmpSchName = "";
		String schoolNameStr = "";
		String merchantId = "";
		String tmpMerchId = "";
		String header = "";
		String responseMessage = "";
		String msisdn = ussdSession.getMsisdn();
		String url = applicationProperties.getDisplaySchListUrl();

		SchNameResp schNameResp = restTemplate.getForObject(url + userInput, SchNameResp.class);
		log.info("response from sadat {}", schNameResp);

		header = schNameResp.getHeader();
		responseMessage = schNameResp.getBody().getResponseMessage();
		if (GeneralResponse.CODE_01_OK.equalsIgnoreCase(schNameResp.getBody().getResponseCode())) {

			List<SchNameData> schNameDataList = schNameResp.getBody().getData();

			if(schNameDataList.size() > 9){
				schoolSize = 9;
			}else{
				schoolSize = schNameDataList.size();
			}
			for (int i = 0; i < schoolSize; i++) {
//				for (int i = 0; i < schNameDataList.size(); i++) {
				schoolName = schNameDataList.get(i).getMerchantName();
//				schoolNameStr += "\n" + (i + 1) + "." + schNameDataList.get(i).getMerchantName();
				merchantId = schNameDataList.get(i).getMerchantId();

				schoolsMap.put(i + 1, merchantId + "," + schoolName);
			}
			log.info("the schools map :{}", schoolsMap);
			log.info("the size of the school map"+schoolsMap.size());
			if(schoolsMap.size() < 6){
				log.info("less than or equal to 5");
				for(int i=1; i<schoolsMap.size(); i++){
					tmpSch = schoolsMap.get(i).split(",");
					schoolNameStr += "\n" + (i) + "." + tmpSch[1];
				}
				ussdResponse.setApplicationResponse(header + schoolNameStr );
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);
				
				ussdSession.setSchoolsMap(schoolsMap);
				ussdSession.setClientState(ClientState.CHOOSE_SCHOOL);
				redisUssdSessionRepository.put(ussdSession);
			}else{
				log.info("greater than or equal to 6");
				for (int i = 1; i < 6; i++) {
					tmpSch = schoolsMap.get(i).split(",");
					schoolNameStr += "\n" + (i) + "." + tmpSch[1];
				}
				ussdResponse.setApplicationResponse(header + schoolNameStr + menuProperties.getDisplayMoreOption());
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);
				
				ussdSession.setSchoolsMap(schoolsMap);
				ussdSession.setClientState(ClientState.MORE_SCHOOLS);
				redisUssdSessionRepository.put(ussdSession);
			}
		} else {
			ussdResponse.setApplicationResponse(header + "\n" + responseMessage + menuProperties.getDisplayTryAgain());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.SCHOOL_NAME_ERROR);
			redisUssdSessionRepository.put(ussdSession);
		}

		return ussdResponse;
	}

	public UssdResponse askUserToChooseMoreSchools(UssdSession ussdSession, String userInput){
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();
		Map<Integer, String> tmpSchoolsMap = new HashMap<>();
		
		String [] tmpSch;
		String header = "";
		String schoolNameStr = "";
		Integer size ;

		switch (userInput){
		case "0":{
			log.info("am in here");
			header = ussdSession.getHeader();
			tmpSchoolsMap = ussdSession.getSchoolsMap();
			
			log.info("the school size: {}",tmpSchoolsMap.size());
			if(tmpSchoolsMap.size() > 10){
				size = 11;
			}else{
				size = tmpSchoolsMap.size();
			}
			for(int i=6 ; i<size+1 ;i++){
				tmpSch = tmpSchoolsMap.get(i).split(",");
				schoolNameStr += "\n" + (i) + "." + tmpSch[1];
			}
			ussdResponse.setApplicationResponse(schoolNameStr );
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);
			
			ussdSession.setClientState(ClientState.CHOOSE_SCHOOL);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
			
		default:{
			log.info("am not in here");
			for (int i = 1; i < 6; i++) {
				tmpSch = tmpSchoolsMap.get(i).split(",");
				schoolNameStr += "\n" + (i) + "." + tmpSch[1];
			}
			ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput() + schoolNameStr + menuProperties.getDisplayMoreOption());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);
			
			ussdSession.setClientState(ClientState.MORE_SCHOOLS);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		}
		return ussdResponse;
	}
	
	public UssdResponse askUserToChooseServiceUsingSchName(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		Map<Integer, String> productsMap = new HashMap<>();

		ussdSession.setSubscriberInput(userInput);
		ussdSession.setSchoolName(userInput);

		String chosenMerchant = "";
		String[] chosenMerchantArr;
		String chosenMerchantId = "";
		String chosenSchoolName = "";
		String productId = "";
		String productName = "";
		String productNameStr = "";
		String header = "";
		String serviceCode = "";
		String apiIndex = "";
		String responseMessage = "";
		String oldSchoolsList = "";
		String[] oldMerchArr;
		String merchantId = "";
		String[] tmpSch;
		String schoolName = "";
		String schoolNameStr = "";
		String msisdn = ussdSession.getMsisdn();
		String url = applicationProperties.getSearchForServByMerchId();

		switch (userInput) {
		case "0": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

		default: {
			Map<Integer, String> schoolsMap = ussdSession.getSchoolsMap();
			if(StringUtils.isNumeric(userInput) == true) 
			chosenMerchant = schoolsMap.get(Integer.parseInt(userInput));

			if (chosenMerchant != null) {
				log.info("the chosen merchant{}", chosenMerchant);
				chosenMerchantArr = chosenMerchant.split(",");
				chosenMerchantId = chosenMerchantArr[0];
				chosenSchoolName = chosenMerchantArr[1];

				log.info("The url : {}", url + chosenMerchantId);
				SchAliasResp servListResp = restTemplate.getForObject(url + chosenMerchantId, SchAliasResp.class);
				log.info("response from sadat {}", servListResp);
				header = servListResp.getHeader();
				responseMessage = servListResp.getBody().getResponseMessage();
				if (GeneralResponse.CODE_01_OK.equalsIgnoreCase(servListResp.getBody().getResponseCode())) {

					List<SchAliasData> servList = servListResp.getBody().getData();

					for (int j = 0; j < servList.size(); j++) {
						productId += servList.get(j).getProductId();
						productName = servList.get(j).getProductName();
						productNameStr += "\n" + (j + 1) + "." + servList.get(j).getProductName();
						serviceCode = servList.get(j).getServiceCode();
						apiIndex = servList.get(j).getApiIndex();

						// Put products into products map and save in session
						productsMap.put((j + 1), servList.get(j).getProductName());
					}

					if (servList.size() != 1) {
						ussdResponse.setApplicationResponse(header + "\n" + chosenSchoolName + productNameStr);
						ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
						ussdResponse.setMsisdn(msisdn);

						// ussdSession.setSchoolAlias(userInput);
						ussdSession.setHeader(header);
						ussdSession.setSchoolName(chosenSchoolName);
						ussdSession.setApiIndex(apiIndex);
						ussdSession.setMerchantId(chosenMerchantId);
						ussdSession.setServiceCode(serviceCode);
						ussdSession.setProductsMap(productsMap);
						ussdSession.setClientState(ClientState.CHOOSE_SERVICE);
						redisUssdSessionRepository.put(ussdSession);
					} else if (servList.size() == 1) {
						ussdResponse.setApplicationResponse(
								MessageFormat.format(menuProperties.getEnterStudenId(), chosenSchoolName, productName));
						ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
						ussdResponse.setMsisdn(msisdn);

						// ussdSession.setSchoolAlias(userInput);
						ussdSession.setSchoolName(chosenSchoolName);
						ussdSession.setApiIndex(apiIndex);
						ussdSession.setMerchantId(chosenMerchantId);
						ussdSession.setServiceCode(serviceCode);
						ussdSession.setProductId(productId);
						ussdSession.setProductName(productName);
						ussdSession.setClientState(ClientState.ENTER_STUDENT_ID);
						redisUssdSessionRepository.put(ussdSession);
					}
				} else {
					ussdResponse.setApplicationResponse(menuProperties.getDisplayEnterSchoolNameAgain());
					ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
					ussdResponse.setMsisdn(msisdn);

					ussdSession.setClientState(ClientState.ENTER_SCHOOL_NAME);
					redisUssdSessionRepository.put(ussdSession);
				}
			} else {
				if(schoolsMap.size() <= 5){
					for (int i = 0; i < schoolsMap.size(); i++) {
						oldSchoolsList = schoolsMap.get(i + 1);
						oldMerchArr = oldSchoolsList.split(",");
						merchantId = oldMerchArr[0];
						schoolName = oldMerchArr[1];
						schoolNameStr += "\n"+ (i + 1) + "." + oldMerchArr[1] ;
					}
					ussdResponse.setApplicationResponse(
							menuProperties.getDisplayInvalidInput() + schoolNameStr + menuProperties.getDisplayCancel());
					ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
					ussdResponse.setMsisdn(msisdn);
					
					ussdSession.setClientState(ClientState.CHOOSE_SCHOOL);
					redisUssdSessionRepository.put(ussdSession);
				}else{
					log.info("greater than 5");
					for (int i = 1; i < 6; i++) {
						tmpSch = schoolsMap.get(i).split(",");
						schoolNameStr += "\n" + (i) + "." + tmpSch[1];
					}
					ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput() + schoolNameStr + menuProperties.getDisplayMoreOption());
					ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
					ussdResponse.setMsisdn(msisdn);
					
					ussdSession.setSchoolsMap(schoolsMap);
					ussdSession.setClientState(ClientState.MORE_SCHOOLS);
					redisUssdSessionRepository.put(ussdSession);
				}
			}
			break;
		}
		}

		return ussdResponse;
	}

	public UssdResponse askUserToEnterStudentId(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();
		ussdSession.setSubscriberInput(userInput);

		switch (userInput) {
		case "0": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;

		}
		default: {
			if (ussdSession.getProductsMap().get(new Integer(userInput)) != null) {
				ussdSession.setProductName(ussdSession.getProductsMap().get(new Integer(userInput)));

				String schoolName = ussdSession.getSchoolName();
				String productName = ussdSession.getProductName();
				// Work with the services that come from sadat
				// Save the user's choice
				ussdResponse.setApplicationResponse(
						MessageFormat.format(menuProperties.getEnterStudenId(), schoolName, productName));
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.ENTER_STUDENT_ID);
				redisUssdSessionRepository.put(ussdSession);
			} else if (AppConstants.SEARCH_BY_ALIAS.equalsIgnoreCase(ussdSession.getSearchCriteria())) {

				String productStr = "";
				String header = ussdSession.getHeader();
				String schoolName = ussdSession.getSchoolName();
				Map<Integer, String> tmpProductsMap = ussdSession.getProductsMap();

				for (int i = 1; i < tmpProductsMap.size() + 1; i++) {
					productStr += "\n" + i + "." + tmpProductsMap.get(i);
				}

				// Try Again
				ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput() + schoolName + productStr + "\n" + menuProperties.getDisplayCancel());
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.CHOOSE_SERVICE);
				redisUssdSessionRepository.put(ussdSession);

				// ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput()+menuProperties.getDisplayTryAgain());
				// ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				// ussdResponse.setMsisdn(msisdn);
				//
				// ussdSession.setClientState(ClientState.ALIAS_SERVICE_ERROR);
				// redisUssdSessionRepository.put(ussdSession);
			} else if (AppConstants.SEARCH_BY_NAME.equalsIgnoreCase(ussdSession.getSearchCriteria())) {
				String productStr = "";
				String header = ussdSession.getHeader();
				String schoolName = ussdSession.getSchoolName();
				Map<Integer, String> tmpProductsMap = ussdSession.getProductsMap();

				for (int i = 1; i < tmpProductsMap.size() + 1; i++) {
					productStr += "\n" + i + "." + tmpProductsMap.get(i);
				}

				// Try Again
				ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput() + schoolName + productStr + "\n" + menuProperties.getDisplayCancel());
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.CHOOSE_SERVICE);
				redisUssdSessionRepository.put(ussdSession);

				// ussdResponse.setApplicationResponse(menuProperties.getDisplayInvalidInput()+menuProperties.getDisplayTryAgain());
				// ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				// ussdResponse.setMsisdn(msisdn);
				//
				// ussdSession.setClientState(ClientState.NAME_SERVICE_ERROR);
				// redisUssdSessionRepository.put(ussdSession);
			}
		}
		}

		return ussdResponse;
	}

	public UssdResponse askUserToEnterAmount(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		StdIdReq stdIdReq = new StdIdReq();

		ussdSession.setSubscriberInput(userInput);

		stdIdReq.setProductId(ussdSession.getProductId());
		stdIdReq.setMerchantId(ussdSession.getMerchantId());
		stdIdReq.setApiIndex(ussdSession.getApiIndex());
		stdIdReq.setSource(applicationProperties.getNetwork());
		stdIdReq.setServiceCode(ussdSession.getServiceCode());
		stdIdReq.setAccountRef(userInput);

		String studentName = "";
		String header = "";
		String responseMessage = "";
		String msisdn = ussdSession.getMsisdn();
		String schoolName = ussdSession.getSchoolName();
		String productName = ussdSession.getProductName();
		String url = applicationProperties.getSearchByStdIdUrl();

		switch (userInput) {
		case "0": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		default: {
			log.info("The request json going :{}", stdIdReq);
			StdIdResp studentResponse = restTemplate.postForObject(url, stdIdReq, StdIdResp.class);
			log.info("response from Sadat : {}", studentResponse);
			header = studentResponse.getHeader();
			responseMessage = studentResponse.getBody().getResponseMessage();
			if (GeneralResponse.CODE_01_OK.equalsIgnoreCase(studentResponse.getBody().getResponseCode())) {

				studentName = studentResponse.getBody().getData().getName();
				ussdResponse.setApplicationResponse(
						MessageFormat.format(menuProperties.getEnterAmount(), schoolName, studentName, productName));
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setStudentId(userInput);
				ussdSession.setStudentName(studentName);
				ussdSession.setClientState(ClientState.ENTER_AMOUNT);
				redisUssdSessionRepository.put(ussdSession);
			} else {
				ussdResponse.setApplicationResponse(menuProperties.getDisplayEnterStudentIdAgain());
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.ENTER_STUDENT_ID);
				redisUssdSessionRepository.put(ussdSession);
			}

		}

		}

		return ussdResponse;
	}

	public UssdResponse askUserToConfirmPayment(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();

		ussdSession.setSubscriberInput(userInput);

		String msisdn = ussdSession.getMsisdn();
		String schoolName = ussdSession.getSchoolName();
		String service = ussdSession.getProductName();
		String studentName = ussdSession.getStudentName();
		String currency = applicationProperties.getCurrencyCode();

		switch (userInput) {
		case "0": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		default: {
			if (StringUtils.isNumeric(userInput) == true) {
				log.info("the amount is Numeric");
				BigDecimal amount = new BigDecimal(userInput);
				amount = amount.setScale(2, BigDecimal.ROUND_HALF_UP);

				log.info("the amount before formatting: {}", amount);

				// Format the amount
				NumberFormat numberFormatter = NumberFormat.getNumberInstance(Locale.US);
				String newAmount = numberFormatter.format(amount);
				log.info("the amount after formatting: {}", newAmount);
				ussdSession.setAmount(amount);

				ussdResponse.setApplicationResponse(MessageFormat.format(menuProperties.getConfirmPayment(), schoolName,
						studentName, service, currency + " " + newAmount));
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.CONFIRM_PAYMENT);
				redisUssdSessionRepository.put(ussdSession);
			} else {
				log.info("the amount is not Numeric");
				ussdResponse.setApplicationResponse(menuProperties.getDisplayEnterAmountAgain());
				ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
				ussdResponse.setMsisdn(msisdn);

				ussdSession.setClientState(ClientState.ENTER_AMOUNT);
				redisUssdSessionRepository.put(ussdSession);
			}
			break;
		}
		}
		return ussdResponse;
	}

	public UssdResponse approveBillPrompt(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();
		ussdSession.setSubscriberInput(userInput);

		switch (userInput) {
		case "1":
			ussdResponse.setApplicationResponse(menuProperties.getApproveBillPrompt());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.APPROVE_BILLPROMPT);
			redisUssdSessionRepository.put(ussdSession);

			String message = JsonUtility.toJson(ussdSession);
			rabbitMQPublisher.publish(message);
			break;

		case "2":
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		return ussdResponse;
	}

	public UssdResponse askUserToEnterSchoolIdAgain(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();
		ussdSession.setSubscriberInput(userInput);

		switch (userInput) {
		// Try Again
		case "1": {
			ussdResponse.setApplicationResponse(menuProperties.getEnterSchoolAlias());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.ENTER_SCHOOL_ALIAS);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// Cancel
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		}
		return ussdResponse;
	}

	public UssdResponse askUserToEnterSchoolNameAgain(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();

		String msisdn = ussdSession.getMsisdn();
		ussdSession.setSubscriberInput(userInput);

		switch (userInput) {
		// Try again
		case "1": {
			ussdResponse.setApplicationResponse(menuProperties.getEnterSchoolName());
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.ENTER_SCHOOL_NAME);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
			// Cancel
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		}
		return ussdResponse;
	}

	public UssdResponse askUserToEnterStudentIdAgain(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		ussdSession.setSubscriberInput(userInput);

		String msisdn = ussdSession.getMsisdn();
		String schoolName = ussdSession.getSchoolName();
		String productName = ussdSession.getProductName();
		switch (userInput) {
		// Try Again
		case "1": {
			ussdResponse.setApplicationResponse(
					MessageFormat.format(menuProperties.getEnterStudenId(), schoolName, productName));
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.ENTER_STUDENT_ID);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// Cancel
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}
		}
		return ussdResponse;
	}

	public UssdResponse askUserToEnterAmountAgain(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();

		String msisdn = ussdSession.getMsisdn();
		String schoolName = ussdSession.getSchoolName();
		String studentName = ussdSession.getStudentName();
		String service = ussdSession.getProductName();

		switch (userInput) {
		// Try Again
		case "1": {
			ussdResponse.setApplicationResponse(
					MessageFormat.format(menuProperties.getEnterAmount(), schoolName, studentName, service));
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.ENTER_AMOUNT);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// Cancel
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

		}
		return ussdResponse;
	}

	public UssdResponse askUserToChooseServiceUsingSchAliasAgain(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();

		String productStr = "";
		String header = ussdSession.getHeader();
		Map<Integer, String> tmpProductsMap = ussdSession.getProductsMap();

		for (int i = 1; i < tmpProductsMap.size() + 1; i++) {
			productStr += "\n" + i + "." + tmpProductsMap.get(i);
		}

		switch (userInput) {
		// Try Again
		case "1": {
			ussdResponse.setApplicationResponse(header + productStr);
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.CHOOSE_SERVICE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// Cancel
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

		}

		return ussdResponse;
	}

	public UssdResponse askUserToChooseServiceUsingSchNameAgain(UssdSession ussdSession, String userInput) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();

		String productStr = "";
		String header = ussdSession.getHeader();
		Map<Integer, String> tmpProductsMap = ussdSession.getProductsMap();

		for (int i = 1; i < tmpProductsMap.size() + 1; i++) {
			productStr += "\n" + i + "." + tmpProductsMap.get(i);
		}

		switch (userInput) {
		// Try Again
		case "1": {
			ussdResponse.setApplicationResponse(header + productStr);
			ussdResponse.setFreeflow(Freeflow.createContinuousFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.CHOOSE_SERVICE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

			// Cancel
		case "2": {
			ussdResponse.setApplicationResponse(menuProperties.getDisplayCancelMessage());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
			ussdResponse.setMsisdn(msisdn);

			ussdSession.setClientState(ClientState.DISPLAY_CANCEL_MESSAGE);
			redisUssdSessionRepository.put(ussdSession);
			break;
		}

		}

		return ussdResponse;
	}

	public UssdResponse killSession(UssdSession ussdSession) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();

		ussdResponse.setApplicationResponse(menuProperties.getDisplayThankYou());
		ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
		ussdResponse.setMsisdn(msisdn);
		redisUssdSessionRepository.delete(ussdSession);
		return ussdResponse;
	}

	public UssdResponse unknownClientState(UssdSession ussdSession) {
		UssdResponse ussdResponse = new UssdResponse();
		String msisdn = ussdSession.getMsisdn();

		ussdResponse.setApplicationResponse(menuProperties.getUnknownClientState());
		ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
		ussdResponse.setMsisdn(msisdn);

		return ussdResponse;
	}
}
