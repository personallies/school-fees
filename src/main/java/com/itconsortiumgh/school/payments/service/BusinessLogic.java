package com.itconsortiumgh.school.payments.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.school.payments.model.AppConstants;
import com.itconsortiumgh.school.payments.model.ClientState;
import com.itconsortiumgh.school.payments.model.Freeflow;
import com.itconsortiumgh.school.payments.model.UssdRequest;
import com.itconsortiumgh.school.payments.model.UssdResponse;
import com.itconsortiumgh.school.payments.model.UssdSession;
import com.itconsortiumgh.school.payments.properties.MenuProperties;
import com.itconsortiumgh.school.payments.repository.Repository;
import com.itconsortiumgh.school.payments.repository.redis.RedisUssdSessionRepository;
import com.itconsortiumgh.school.payments.utils.UssdRequestToXmlConverter;
import com.itconsortiumgh.school.payments.utils.UssdResponseToXmlConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BusinessLogic {
	@Autowired
	MenuProperties menuProperties;
	@Autowired 
	UserInputProcessor userInputProcessor;
	@Autowired
	Repository repository;
	@Autowired 
	RedisUssdSessionRepository redisUssdSessionRepository;
	
	public String paySchoolFees(String xmlRequest){
		//Convert xml request from MTN to ussdRequest
		UssdRequest ussdRequest = UssdRequestToXmlConverter.convertXmlToUssdRequest(xmlRequest);
		
		//process request to get a response
		String newRequest = ussdRequest.getNewRequest();
		String sessionId = ussdRequest.getSessionId();
		String userInput = ussdRequest.getSubscriberInput();
		String msisdn = ussdRequest.getMsisdn();
		
		UssdResponse ussdResponse = new UssdResponse();
		UssdSession ussdSession = null;
		//Checking whether screen is the first one
		if(AppConstants.NEW_REQUEST.equalsIgnoreCase(newRequest)){
			ussdSession = new UssdSession(sessionId);
			ussdSession.setMsisdn(msisdn);
			ussdSession.setClientState(ClientState.ROOT);
			//Switch with the ClientStates
			ussdResponse = switchAndProcessClientState(ussdSession, userInput);
		}else if(AppConstants.CONTINUING_REQUEST.equalsIgnoreCase(newRequest)){
			ussdSession = redisUssdSessionRepository.get(sessionId);
			ussdSession.setMsisdn(msisdn);
			//Switch with the ClientStates
			ussdResponse = switchAndProcessClientState(ussdSession, userInput);
		}else{
			ussdResponse.setApplicationResponse(menuProperties.getDisplayUnavailableSession());
			ussdResponse.setFreeflow(Freeflow.createTerminalFreeflow());
		}
		
		
		//Convert ussdResponse to xml response and return to MTN
		String responseXml = UssdResponseToXmlConverter.convertResponseToXml(ussdResponse);
		return responseXml;
	}
	
	public UssdResponse switchAndProcessClientState(UssdSession ussdSession, String userInput){
		UssdResponse ussdResponse = new UssdResponse();
		String clientState = ussdSession.getClientState();
		String msisdn = ussdSession.getMsisdn();
//		ussdResponse.setMsisdn(msisdn);
		log.info("What the user entered:{}",userInput);
		log.info("the ussd session:{}",ussdSession);
		
		switch (clientState){
		case ClientState.ROOT:
			ussdResponse = userInputProcessor.askUserToChooseSearchCriteria(ussdSession);
			break;
			
		case ClientState.DISPLAY_FIRST_MENU:
			ussdResponse = userInputProcessor.askUserToEnterSearchInput(ussdSession, userInput);
			break;
			
		case ClientState.ENTER_SCHOOL_ALIAS:
			ussdResponse = userInputProcessor.askUserToChooseServiceUsingSchAlias(ussdSession, userInput);
			break;
			
		case ClientState.ENTER_SCHOOL_NAME:
			ussdResponse = userInputProcessor.askUserToChooseSchool(ussdSession, userInput);
			break;
			
		case ClientState.MORE_SCHOOLS:
			ussdResponse = userInputProcessor.askUserToChooseMoreSchools(ussdSession, userInput);
			break;
			
		case ClientState.CHOOSE_SCHOOL:
			ussdResponse = userInputProcessor.askUserToChooseServiceUsingSchName(ussdSession, userInput);
			break;
			
		case ClientState.CHOOSE_SERVICE:
			ussdResponse = userInputProcessor.askUserToEnterStudentId(ussdSession, userInput);
			break;
			
		case ClientState.ENTER_STUDENT_ID:
			ussdResponse = userInputProcessor.askUserToEnterAmount(ussdSession, userInput);
			break;
			
		case ClientState.ENTER_AMOUNT:
			ussdResponse = userInputProcessor.askUserToConfirmPayment(ussdSession, userInput);
			break;
			
		case ClientState.CONFIRM_PAYMENT:
			ussdResponse = userInputProcessor.approveBillPrompt(ussdSession, userInput);
			break;
			
		case ClientState.APPROVE_BILLPROMPT:
			userInputProcessor.killSession(ussdSession);
			break;
			
		case ClientState.SCHOOL_ALIAS_ERROR:
			ussdResponse = userInputProcessor.askUserToEnterSchoolIdAgain(ussdSession, userInput);
			break;
			
		case ClientState.SCHOOL_NAME_ERROR:
			ussdResponse = userInputProcessor.askUserToEnterSchoolNameAgain(ussdSession, userInput);
			break;
			
		case ClientState.STUDENT_ERROR:
			ussdResponse = userInputProcessor.askUserToEnterStudentIdAgain(ussdSession, userInput);
			break;
			
		case ClientState.AMOUNT_ERROR:
			ussdResponse = userInputProcessor.askUserToEnterAmountAgain(ussdSession, userInput);
			break; 
			
		case ClientState.ALIAS_SERVICE_ERROR:
			ussdResponse = userInputProcessor.askUserToChooseServiceUsingSchAliasAgain(ussdSession, userInput);
			break;
			
		case ClientState.NAME_SERVICE_ERROR:
			ussdResponse = userInputProcessor.askUserToChooseServiceUsingSchNameAgain(ussdSession, userInput);
			break;
			
		default:
			ussdResponse = userInputProcessor.unknownClientState(ussdSession);
			break;
		}
		
		repository.save(ussdSession);
		return ussdResponse;
	}
	
	
}
