package com.itconsortiumgh.school.payments.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.itconsortiumgh.school.payments.service.BusinessLogic;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
//@RequestMapping("/pay")
public class Controller {
	@Autowired
	BusinessLogic businessLogic;
	
	@PostMapping(value="/redirect", produces=MediaType.APPLICATION_XML_VALUE, consumes={MediaType.APPLICATION_XML_VALUE, MediaType.TEXT_XML_VALUE})
	public String paySchoolFees(@RequestBody String xmlRequest){
		log.info("***********xml request \n{}",xmlRequest);
		String responseXml = businessLogic.paySchoolFees(xmlRequest);
		return responseXml;
	}

}
