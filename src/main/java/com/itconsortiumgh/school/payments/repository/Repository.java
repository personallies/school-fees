package com.itconsortiumgh.school.payments.repository;

import org.springframework.data.repository.CrudRepository;

import com.itconsortiumgh.school.payments.model.UssdSession;

public interface Repository extends CrudRepository<UssdSession, Long> {

}
