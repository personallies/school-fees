package com.itconsortiumgh.school.payments.repository.redis;

import java.util.List;

import com.itconsortiumgh.school.payments.model.DomainObject;

public interface RedisRepository<V extends DomainObject> {
	 void put(V obj);
	 V get(V key);
	 void delete(V key);
	 List<V> getObjects();
}
