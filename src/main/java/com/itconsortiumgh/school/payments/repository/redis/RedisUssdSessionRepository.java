package com.itconsortiumgh.school.payments.repository.redis;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.itconsortiumgh.school.payments.model.UssdSession;
import com.itconsortiumgh.school.payments.properties.ApplicationProperties;
import com.itconsortiumgh.school.payments.utils.JsonUtility;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Repository
public class RedisUssdSessionRepository implements RedisRepository<UssdSession>{
	@Autowired
	ApplicationProperties applicationProperties;
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public void put(UssdSession ussdSession) {
		// TODO Auto-generated method stub
		log.info("ussdsession.getObjectKey: {}, ussdsession.getKey: {}", ussdSession.getObjectKey(), ussdSession.getKey());
		log.info("ussdsession {}", ussdSession);
		//		redisTemplate.opsForHash().put(ussdSession.getObjectKey(), ussdSession.getKey(), JsonUtility.toJson(ussdSession));
		redisTemplate.opsForValue().set(ussdSession.getKey(), JsonUtility.toJson(ussdSession));
		Boolean expire = redisTemplate.expire(ussdSession.getKey(), applicationProperties.getUssdSessionTimeout(), TimeUnit.MINUTES);
		log.info("Expire {}", expire);
		log.info("=======Expire period========{}", redisTemplate.getExpire(ussdSession.getKey()));
	}

	@Override
	public UssdSession get(UssdSession key) {
		// TODO Auto-generated method stub
		//		 return (UssdSession) redisTemplate.opsForHash().get(key.getObjectKey(),
		//		key.getKey());
		String json = redisTemplate.opsForValue().get(key.getKey());
		UssdSession ussdSession = null;
		try {
			if(json!=null) {
			 ussdSession = JsonUtility.fromJson(json, UssdSession.class);
			 
			}else {
				return null;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ussdSession;
	}
	
	public UssdSession get(String key) {
		//		 return (UssdSession) redisTemplate.opsForHash().get(key.getObjectKey(),
		//				key.getKey());
		String json = redisTemplate.opsForValue().get("ScP_"+key);
		UssdSession ussdSession = null;
		try {
			if(json!=null) {
				ussdSession = JsonUtility.fromJson(json, UssdSession.class);
				
			}else {
				return null;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ussdSession;
	}

	@Override
	public void delete(UssdSession key) {
		// TODO Auto-generated method stub
		redisTemplate.opsForValue().getOperations().delete(key.getKey());
		//		redisTemplate.delete(key.getKey());
	}

	@Override
	public List<UssdSession> getObjects() {
		// TODO Auto-generated method stub
		List<UssdSession> ussdSessions = new ArrayList<UssdSession>();
		for (Object ussdSession : redisTemplate.opsForHash().values(UssdSession.OBJECT_KEY) ){
			ussdSessions.add((UssdSession)ussdSession);
		}
		return ussdSessions;
	}
}
