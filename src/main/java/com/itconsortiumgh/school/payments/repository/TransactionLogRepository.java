package com.itconsortiumgh.school.payments.repository;

import org.springframework.data.repository.CrudRepository;

import com.itconsortiumgh.school.payments.model.TransactionLogs;

public interface TransactionLogRepository extends CrudRepository<TransactionLogs, Long> {

}
