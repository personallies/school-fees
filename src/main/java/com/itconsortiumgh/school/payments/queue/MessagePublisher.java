package com.itconsortiumgh.school.payments.queue;

public interface MessagePublisher {

    void publish(final String message);
}