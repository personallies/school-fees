package com.itconsortiumgh.school.payments.queue;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.itconsortiumgh.school.payments.model.AppConstants;
import com.itconsortiumgh.school.payments.model.InvoiceResponse;
import com.itconsortiumgh.school.payments.model.TransactionLogs;
import com.itconsortiumgh.school.payments.model.UssdSession;
import com.itconsortiumgh.school.payments.properties.ApplicationProperties;
import com.itconsortiumgh.school.payments.utils.JsonUtility;

import gh.com.dialect.transflow.webclient.integration.model.xsd.PostInvResp;
import gh.com.dialect.transflow.webclient.services.InvoicingService;
import gh.com.dialect.transflow.webclient.services.InvoicingServicePortType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RabbitMQSubscriber implements MessageListener{
	@Autowired
	ApplicationProperties applicationProperties;
	@Autowired
	RestTemplate restTemplate;
	
    private CountDownLatch latch = new CountDownLatch(1);
    
	@Override
	public void onMessage(Message incomingMessage) {
		// TODO Auto-generated method stub
		Long delay1 = new Long(applicationProperties.getDelayBeforePush().trim());
		long delay  = delay1.longValue();
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String message = new String(incomingMessage.getBody());
		processMessage(message);
	}

    public void processMessage(String message) {
        System.out.println("Received <" + message + ">");
        UssdSession ussdSession = JsonUtility.fromJson(message, UssdSession.class);
        TransactionLogs transactionLogs = new TransactionLogs(ussdSession);
        
        //format expiry date YYYY-MM-DD
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date()); 
		c.add(Calendar.DATE,applicationProperties.getExpiryDays()); 
		String expiry = sdf.format(c.getTime());
		String xmlLocation = applicationProperties.getXmlLocation();
        
		URL url = null;
		try{
			url = new URL(xmlLocation.trim());
		}catch(MalformedURLException e){
			e.printStackTrace();
		}
        
        String product = ussdSession.getProductName();
//        String amount = ussdSession.getAmount().toString(); 
        
//        InvoiceRequest invoiceRequest = new InvoiceRequest();
//        invoiceRequest.setName(applicationProperties.getServiceName());
//        invoiceRequest.setInfo(MessageFormat.format(applicationProperties.getNarration(), ussdSession.getSchoolName(), ussdSession.getStudentName(), product, amount));
//        invoiceRequest.setAmt(amount);
//        invoiceRequest.setMobile(ussdSession.getMsisdn());
//        invoiceRequest.setMesg(MessageFormat.format(applicationProperties.getSmsTemplate(), applicationProperties.getNetwork(), amount, product));
//        invoiceRequest.setExpiry(expiryDate);
//        invoiceRequest.setBillprompt(applicationProperties.getBillPrompt());
//        invoiceRequest.setThirdpartyID(applicationProperties.getThirdPartyID());
//        invoiceRequest.setUsername(applicationProperties.getUsername());
//        invoiceRequest.setPassword(applicationProperties.getPassword());
        
        String name = applicationProperties.getServiceName();
        Double amt = ussdSession.getAmount().doubleValue();
        String info = MessageFormat.format(applicationProperties.getNarration(), ussdSession.getSchoolName(), ussdSession.getStudentName(), product, amt);
        String mobile = ussdSession.getMsisdn();
        String mesg = MessageFormat.format(applicationProperties.getSmsTemplate(), applicationProperties.getNetwork(), amt, product);
        String billprompt = applicationProperties.getBillPrompt();
        String thirdpartyID = applicationProperties.getThirdPartyID();
        String username = applicationProperties.getUsername();
        String password = applicationProperties.getPassword();
        
//        InvoiceResponse invoiceResponse = null;
//        String url=applicationProperties.getTransflowUrl();
        
        try{
//        	log.info("the request going: {}",invoiceRequest);
        	InvoicingService invoice = new InvoicingService(url);
        	InvoicingServicePortType in = invoice.getInvoicingServiceHttpSoap11Endpoint();	
        	PostInvResp response = in.postInvoice(name, info, amt, mobile, mesg, expiry, billprompt, thirdpartyID, username, password);
//        	invoiceResponse = restTemplate.postForObject(url, invoiceRequest, InvoiceResponse.class);
			log.info("Invoice number {}",response.getInvoiceNo().getValue());
			log.info("The response code {}",response.getResponseCode().getValue());
			log.info("The response message {}",response.getResponseMessage().getValue());
        	log.info("the response coming: {}", response.toString());
        	
        	if(AppConstants.TRANSFLOW_SUCCESS.equalsIgnoreCase(response.getResponseCode().getValue())){
        		transactionLogs.setInvoiceNo(response.getInvoiceNo().getValue());
        		log.info("Bill Prompt successful");
        	}else{
        		log.info("Bill Prompt unsuccessful");
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	log.error("{}",e);
        }
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }


}
