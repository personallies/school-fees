package com.itconsortiumgh.school.payments.queue;

import java.util.concurrent.TimeUnit;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.stereotype.Service;

import com.itconsortiumgh.school.payments.SchoolPaymentsUssdV1Application;

import lombok.Data;

@Data
@Component
@Service
public class RabbitMQPublisher implements MessagePublisher{
	private final RabbitTemplate rabbitTemplate;
	private final RabbitMQSubscriber rabbitMqSubscriber;
	private final ConfigurableApplicationContext context;
	
	public RabbitMQPublisher(RabbitMQSubscriber rabbitMqSubscriber, RabbitTemplate rabbitTemplate,
            ConfigurableApplicationContext context){
		this.rabbitMqSubscriber = rabbitMqSubscriber;
		this.rabbitTemplate = rabbitTemplate;
		this.context = context;
	}

	@Override
	public void publish(String message) {
		// TODO Auto-generated method stub
		System.out.println("Sending message...");
		
		try {
			rabbitTemplate.convertAndSend(SchoolPaymentsUssdV1Application.queueName, message);
			rabbitMqSubscriber.getLatch().await(10000, TimeUnit.MILLISECONDS);
//			context.close();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
