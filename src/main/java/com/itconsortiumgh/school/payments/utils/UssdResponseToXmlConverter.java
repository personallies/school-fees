package com.itconsortiumgh.school.payments.utils;

import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.itconsortiumgh.school.payments.model.UssdResponse;


public class UssdResponseToXmlConverter {
	public static String convertResponseToXml(UssdResponse ussdResponse) {
		/* init jaxb marshaler */
		JAXBContext jaxbContext = null;
		String resultingXML = "";
		try {
			jaxbContext = JAXBContext.newInstance(UssdResponse.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			jaxbMarshaller.marshal(ussdResponse, byteArrayOutputStream);
			String charset="UTF-8";
			resultingXML = byteArrayOutputStream.toString(Charset.defaultCharset().name());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/* set this flag to true to format the output */
		return resultingXML;
	}

}
