package com.itconsortiumgh.school.payments.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.itconsortiumgh.school.payments.model.UssdRequest;


public class UssdRequestToXmlConverter {
//	public static void main(String args[]) {
//		
//	}
	
	public static UssdRequest convertXmlToUssdRequest(String xmlRequest) {
		JAXBContext jaxbContext = null;
		try {
			jaxbContext = JAXBContext.newInstance(UssdRequest.class);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		InputStream requestAsStream = new ByteArrayInputStream(xmlRequest.getBytes(Charset.forName("UTF-8")));

		Unmarshaller jaxbUnmarshaller = null;
		UssdRequest ussdRequest = null;
		try {
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			ussdRequest = (UssdRequest)jaxbUnmarshaller.unmarshal(requestAsStream);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return ussdRequest;
	}
	
	public static String convertRequestToXml(UssdRequest ussdRequest) {
		/* init jaxb marshaler */
		JAXBContext jaxbContext = null;
		String resultingXML = "";
		try {
			jaxbContext = JAXBContext.newInstance(UssdRequest.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			jaxbMarshaller.marshal(ussdRequest, byteArrayOutputStream);
			String charset="UTF-8";
			resultingXML = byteArrayOutputStream.toString(Charset.defaultCharset().name());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/* set this flag to true to format the output */
		return resultingXML;
	}
}
